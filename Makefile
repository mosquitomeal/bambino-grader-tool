no-debug:
	g++ -std=c++17 src/bambino_grader_tool.cpp -o bin/bambino-grader-tool -lncursesw
with-debug:
	g++ -std=c++17 -ggdb3 -O0 src/bambino_grader_tool.cpp -o bin/bambino-grader-tool -lncursesw
