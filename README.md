![](https://i.imgur.com/3x79hVs.png)

## Introduction

A command-line program for helping faculty at King Saud University fill in student grades in Microsoft Excel and to automatically fill the Edugate grades online.

The program offers two modes:

* Excel - for entering grades from paper into excel sheet
* Generate edugate script - for entering grades from excel sheet into edugate forms

For any use, it's best to create a separate clean directory locally to store and work with your files.

## Download the tool

[bambino-grader-tool v2.0](https://gitlab.com/mosquitomeal/bambino-grader-tool/-/raw/main/developer-builds/bambino-grader-tool)

## How to use

1. Create a clean empty directory.
2. Download (or move, if downloaded) the tool in this directory.

...continued...
### Excel mode

3. Create a copy of the original excel workbook in this directory.
4. Open the excel workbook copy in libreoffice -> File menu -> Save as... -> choose "Text CSV (.csv)" in the dropdown menu -> Save in the working directory -> (if prompted) Confirm Use Text CSV format -> Character set: UTF-8, Field delimiter: Comma OR Tab as you wish -> OK

![](https://i.imgur.com/dqGOPXF.png)

5. Run the tool -> Select the above CSV file -> tab/comma as used -> Excel
6. Use the right/left arrow keys and enter on the column asked for. When the tool asks for the "columns TO-GRADE", enter on them IN THE ORDER you want to grade them. This order is used for prompts to enter each student's grades. Once you finish defining the columns you want to grade, press g.
7. Use the up/down arrow keys and enter on the row of the first student, where tasalsul is "1".
8. An infinite cyclic prompt comes up. Enter the last four digits of the student's id, then enter in the prompts for grading (order depends on Step 6). Then again you reach the last four digits prompt. Keep going till you're done.
9. Press either s (if you wish to NOT overwrite the csv and want to create modified copy) OR press o (if you DO wish to overwrite DANGER DANGER!!!). Once done, press q.

![](https://i.imgur.com/dxP2LLd.png)

10. Now open the saved csv from Step 9 and copy relevant cell data into the original excel workbook.

### Generate edugate script mode

3. Open the (now completed) excel workbook in libreoffice and delete all columns except five (tasalsul, id, student name, semester, final, total) and then remove all headers, such that 1A is the the first student's tasalsul (value would be 1). Now you should only have data rows, for the five above mentioned columns.

![](https://i.imgur.com/jzT2Gw5.png)

4. File menu -> Save as... -> choose "Text CSV (.csv)" in the dropdown menu -> Save in the working directory -> (if prompted) Confirm Use Text CSV format -> Character set: UTF-8, Field delimiter: Comma OR Tab as you wish -> OK

![](https://i.imgur.com/hsKJto7.png)

5. Run the tool -> Select the above CSV file -> tab/comma as used -> Generate edugate script -> Press q
6. A new JavaScript file is created, with the same name as the csv used, plus ".js" at the end. Open this file in a text editor and copy all the content.

![](https://i.imgur.com/zLiuh9p.png)

7. In Firefox, go to Edugate website, open the relevant section's form and right-click -> Inspect (Q) -> Go to Console tab -> Paste all the javascript -> Hit run.
8. Confirm if the filled in values are accurate. If yes, submit.

![](https://i.imgur.com/onyDPIY.png)
