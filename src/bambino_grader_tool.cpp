#include <iostream>
#include <ios>
#include <string>
#include <filesystem>
#include <algorithm>
#include <vector>
#include <numeric>
#include <fstream>
#include <sstream>
#include <locale.h>
#include <ncursesw/ncurses.h>
namespace fs = std::filesystem;

// Spreadsheet related data
char delimiter = '	'; // delimiter for properly reading csv file
std::string filename;
std::vector<std::string> spreadsheet; // stores spreadsheet table into 1D vector. To refer [row,column], use spreadsheet[row*columncount+column] 
std::vector<int> cellwidths; // stores number of characters in cell's UTF-8 strings. Used to display columns of suitable width.
int rowcount = 0, columncount = 0; // spreadsheet row count and column count
std::vector<std::string> hazards; // info about columns - like id, name, and to-grade
int idcolumn=0, namecolumn=0;
std::vector<int> columnstograde; // ordered vector of column indices in grading order

// ncurses navigation related vars
bool HAS_QUIT = false; // checked upon return to main() from screen display function calls. If true, program exits.
int showWin = 0; // determines which screen to display. Numbers associate with different screens.
int leftmostcolumn=0;
int rightmostcolumn=5;

// spreadsheet file related functions
void extractSpreadsheetFrom(std::fstream& file); // checks if file contains csv data; populates spreadsheet vector and cellwidths vector, if so.
int countUTF8Chars(std::string input); // count UTF-8 encoded characters in string, with multibyte support
static bool endsWith(const std::string& str, const char* suffix, unsigned suffixlen);
static bool startsWith(const std::string& str, const char* prefix, unsigned prefixLen);
void exportcsv(int option);
void exportjs(std::ofstream& file);

void printHead();
void filechoosescr();
void filechooserscr_update(int selrow, int topoffset, int curlin, std::vector<std::string> &files);
void tabcommascr();
void tabcommascr_update(int selrow, int topoffset, int curlin, std::vector<std::string> &options);
void modeselectscr();
void modeselectscr_update(int selrow, int topoffset, int curlin, std::vector<std::string> &options);
void printSpreadsheetPromptOrder(int& offset, int& numofrows, int* columnwidths, int& selcolumn);
void inputsetupscr(std::string instructions);
void inputsetupscr_update(int selcolumn, int numofrows, std::string instructions);
void gradingscr();
std::vector<int> studentsfound(std::string id);
void printFirstRowSelector(int offset, int numofrows, int &selrow);
void printstudentgrader(int idrow, int firstdatarow);
void printstudentselector(int &topoffset, std::vector<int> &foundones, int &selrow);

int main() {

	// set up and start ncurses environment
	
	setlocale(LC_ALL, ""); // set locale
	initscr(); // start ncurses
	noecho(); // don't print user input
	cbreak(); // submit input buffer on single key press
	curs_set(0); // hide cursor
	refresh(); // refresh stdscr (the default) window
	int h, w; // height and width (in lines and characters) of stdscr window
    getmaxyx(stdscr, h, w); // set h and w
    keypad(stdscr,TRUE); // allow handling special keys, like up, down etc.
	if (has_colors() == TRUE) {
		start_color(); // start color mode
		init_pair(1, COLOR_WHITE, COLOR_BLACK); // initialize some color pairs
		init_pair(2, COLOR_BLACK, COLOR_WHITE); // ...
		init_pair(3, COLOR_CYAN, COLOR_BLACK); // ...
		init_pair(4, COLOR_BLACK, COLOR_CYAN); // ...
		init_pair(5, COLOR_BLACK, COLOR_BLACK);
		init_pair(6, COLOR_MAGENTA, COLOR_BLACK);
		bkgd(COLOR_PAIR(1)); // set default window color pair as white(foreground) and black(background)
    }

	while(!HAS_QUIT) {   // keep in loop as long as application has not quit...
		switch(showWin) {   // check which screen to display in current run of loop 
			case 0:
				filechoosescr();
				break;
			case 1:
				inputsetupscr("Use the < > arrow keys and press enter on the ID column:");
				break;
			case 2:
				gradingscr();
				break;
		}
	}

	//getch(); // take final key press to confirm exit from ncurses
	endwin(); // end ncurses
	
	return 0;
}

void extractSpreadsheetFrom(std::fstream& file) {
	std::string row, content; // row stores current line, that stringstream reads into buffer from csv file; content stores current token from tokenized comma separated row
	int tmpcolumncount = 0;
	while(getline(file, row)) {
		if(row[row.size()-1] == delimiter) { row.append(1, delimiter); } // fixes column counting problem when first row ends with empty cell...
		std::stringstream m(row);
		while(getline(m, content, delimiter)) {   // tokenize row using ',' as separator
			spreadsheet.emplace_back(content); // add content to spreadsheet vector
			
			cellwidths.emplace_back(countUTF8Chars(content)); // count characters in the UTF-8 string, content, and add it to cellwidths vector
			
			tmpcolumncount++;
		}
		if(columncount<tmpcolumncount) { columncount = tmpcolumncount; }
		tmpcolumncount=0;
		std::stringstream().swap(m);
		rowcount++;
	}
	
}

int countUTF8Chars(std::string input) {
	int numofbytes = 0;
	for(size_t j=0; j < input.length();) {
		int cplen = 1;
		// mask bits of input[j] and compare with different UTF-8 continuation bits to determine numofbytes in this UTF-8 char
		if((input[j] & 0xf8) == 0xf0) cplen = 4;
		else if((input[j] & 0xf0) == 0xe0) cplen = 3;
		else if((input[j] & 0xe0) == 0xc0) cplen = 2;
		if((j + cplen) > input.length()) cplen = 1;

		j += cplen; // jump numofbytes many bytes, to next char in input
		numofbytes++;
	}
	return numofbytes;
}

void printHead() {
	mvaddstr(1,1, "Bambino's Grader Tool v2.0, GCC 10.2.1 Build, as of November 11, 2022");
	mvaddstr(2,1, "Linked with ncursesw libraries, with UTF-8 encoding support.");
	mvaddstr(3,1, "Made with love <3, by mommy @redherkools.");
	mvaddstr(5,1, "We begin and end, with the Name of Allah, the Most High...");
}

void filechoosescr() {

	int selrow = 0;
	int topoffset = 6, curlin = topoffset+3;
	
	erase();
	printHead();
	
	mvaddstr(topoffset+2,1, "Select your .csv file:");
	
	std::vector<std::string> files; // stores .csv files found in current directory
	std::string selFile = ""; // user selected file
	int entries = 0; // file entries counter, to set visual properties of printed rows
	
	for (const auto &entry : fs::directory_iterator(".")) {   // add found files in current directory to files vector
		if (entry.path().extension() == ".csv") {
			files.emplace_back(entry.path().filename().u8string().c_str());
		}
	}
	
	// print files found, one per row
	
	for(int i=0 ; i<files.size() ; i++) {
		if(entries == selrow) {   // if current entry is selected BUT NOT highlighted
			attron(COLOR_PAIR(2)); // set color pair as black(foreground) and white(background)
			mvaddstr(curlin++,4, files[i].c_str()); // ...
			attroff(COLOR_PAIR(2)); // ...
		}
		else   // if neither selected nor highlighted
			mvaddstr(curlin++,4, files[i].c_str()); // print with default color pair
		
		entries++;
	}
	
	mvaddstr(topoffset+2+entries+2,1, "[Press q to quit]");
	
	refresh();
	
	int ch; // stores user input in int
	while(ch = getch()) {   // keep getting user input, as long as he hasn't selected Quit option, which would return to main()
		switch(ch) {
			case KEY_UP:
				if(selrow == 0) { // if selected row is top most
					selrow = entries-1; // jump selected row to last file
					filechooserscr_update(selrow, topoffset, topoffset+3, files);
				}
				else { 
					selrow--; // move one row up
					filechooserscr_update(selrow, topoffset, topoffset+3, files);
				}
				break;
			case KEY_DOWN:
				if(selrow == entries-1){ // if selected row is last file
					selrow = 0; // jump selected row to top most file's
					filechooserscr_update(selrow, topoffset, topoffset+3, files);
				}
				else {
					selrow++; // move one row down
					filechooserscr_update(selrow, topoffset, topoffset+3, files);
				}
				break;
			case 10: {  // if ch is ENTER key
				std::fstream file (files[selrow].c_str(), std::ios::in); // open selected file
				if(file.is_open()) {
					filename = files[selrow];
					//tabcommascr
					tabcommascr();
					extractSpreadsheetFrom(file);
					//modeselect
					modeselectscr();
					return;
				}
				else
					mvaddstr(20,1, "couldn't open...");
				refresh();
				break;
			}
			case 113:  // ch is 'q'
			case 81:  // ch is 'Q'
				HAS_QUIT=true;
				return;
				break;
		}
		refresh();
	}
}

void filechooserscr_update(int selrow, int topoffset, int curlin, std::vector<std::string> &files) {
	
	erase(); refresh(); // erase screen to redraw everything based on user selection in caller function
	
	printHead();
	
	mvaddstr(topoffset+2,1, "Select your .csv file:");
	
	int entries = 0; // file entries counter, to set visual properties of printed rows
	
	// print files found, one per row
	
	for(int i=0 ; i<files.size() ; i++) {
		if(entries == selrow) {   // if current entry is selected BUT NOT highlighted
			attron(COLOR_PAIR(2)); // set color pair as black(foreground) and white(background)
			mvaddstr(curlin++,4, files[i].c_str()); // ...
			attroff(COLOR_PAIR(2)); // ...
		}
		else   // if neither selected nor highlighted
			mvaddstr(curlin++,4, files[i].c_str()); // print with default color pair
		
		entries++;
	}
	
	mvaddstr(topoffset+2+entries+2,1, "[Press q to quit]");
	
	refresh();
}

void tabcommascr() {
	int selrow = 0;
	int topoffset = 6, curlin = topoffset+3;
	
	erase();
	printHead();
	
	mvaddstr(topoffset+2,1, "Tab or comma based?:");
	int entries = 0;
	
	std::vector<std::string> options = {"tab [   ]", "comma [,]"};
	
	for(int i=0 ; i<options.size() ; i++) {
		if(entries == selrow) {   // if current entry is selected BUT NOT highlighted
			attron(COLOR_PAIR(2)); // set color pair as black(foreground) and white(background)
			mvaddstr(curlin++,4, options[i].c_str()); // ...
			attroff(COLOR_PAIR(2)); // ...
		}
		else   // if neither selected nor highlighted
			mvaddstr(curlin++,4, options[i].c_str()); // print with default color pair
		
		entries++;
	}
	
	mvaddstr(topoffset+2+entries+2,1, "[Press q to quit]");
	
	refresh();
	
	int ch; // stores user input in int
	while(ch = getch()) {   // keep getting user input, as long as he hasn't selected Quit option, which would return to main()
		switch(ch) {
			case KEY_UP:
				if(selrow == 0) { // if selected row is top most
					selrow = entries-1; // jump selected row to last file
					tabcommascr_update(selrow, topoffset, topoffset+3, options);
				}
				else { 
					selrow--; // move one row up
					tabcommascr_update(selrow, topoffset, topoffset+3, options);
				}
				break;
			case KEY_DOWN:
				if(selrow == entries-1){ // if selected row is last file
					selrow = 0; // jump selected row to top most file's
					tabcommascr_update(selrow, topoffset, topoffset+3, options);
				}
				else {
					selrow++; // move one row down
					tabcommascr_update(selrow, topoffset, topoffset+3, options);
				}
				break;
			case 10: {  // if ch is ENTER key
				if(selrow == 0) {
					delimiter = '	';
				}
				else {
					delimiter = ',';
				}
				return;
				break;
			}
			case 113:  // ch is 'q'
			case 81:  // ch is 'Q'
				HAS_QUIT=true;
				return;
				break;
		}
		refresh();
	}
}

void tabcommascr_update(int selrow, int topoffset, int curlin, std::vector<std::string> &options) {
	erase(); refresh(); // erase screen to redraw everything based on user selection in caller function
	
	printHead();
	
	mvaddstr(topoffset+2,1, "Tab or comma based?:");
	
	int entries = 0; // file entries counter, to set visual properties of printed rows
	
	// print options found, one per row
	
	for(int i=0 ; i<options.size() ; i++) {
		if(entries == selrow) {   // if current entry is selected BUT NOT highlighted
			attron(COLOR_PAIR(2)); // set color pair as black(foreground) and white(background)
			mvaddstr(curlin++,4, options[i].c_str()); // ...
			attroff(COLOR_PAIR(2)); // ...
		}
		else   // if neither selected nor highlighted
			mvaddstr(curlin++,4, options[i].c_str()); // print with default color pair
		
		entries++;
	}
	
	mvaddstr(topoffset+2+entries+2,1, "[Press q to quit]");
	
	refresh();
}

void modeselectscr() {
	int selrow = 0;
	int topoffset = 6, curlin = topoffset+3;
	
	erase();
	printHead();
	
	mvaddstr(topoffset+2,1, "Mode select:");
	int entries = 0;
	
	std::vector<std::string> options = {"Excel", "Generate edugate script"};
	
	for(int i=0 ; i<options.size() ; i++) {
		if(entries == selrow) {   // if current entry is selected BUT NOT highlighted
			attron(COLOR_PAIR(2)); // set color pair as black(foreground) and white(background)
			mvaddstr(curlin++,4, options[i].c_str()); // ...
			attroff(COLOR_PAIR(2)); // ...
		}
		else   // if neither selected nor highlighted
			mvaddstr(curlin++,4, options[i].c_str()); // print with default color pair
		
		entries++;
	}
	
	mvaddstr(topoffset+2+entries+2,1, "[Press q to quit]");
	
	refresh();
	
	int ch; // stores user input in int
	while(ch = getch()) {   // keep getting user input, as long as he hasn't selected Quit option, which would return to main()
		switch(ch) {
			case KEY_UP:
				if(selrow == 0) { // if selected row is top most
					selrow = entries-1; // jump selected row to last file
					modeselectscr_update(selrow, topoffset, topoffset+3, options);
				}
				else { 
					selrow--; // move one row up
					modeselectscr_update(selrow, topoffset, topoffset+3, options);
				}
				break;
			case KEY_DOWN:
				if(selrow == entries-1){ // if selected row is last file
					selrow = 0; // jump selected row to top most file's
					modeselectscr_update(selrow, topoffset, topoffset+3, options);
				}
				else {
					selrow++; // move one row down
					modeselectscr_update(selrow, topoffset, topoffset+3, options);
				}
				break;
			case 10: {  // if ch is ENTER key
				if(selrow == 0) {
					showWin = 1;
					return;
				}
				else {
					std::stringstream s;
					s << filename << ".js";
					std::ofstream jsfile(s.str());
					exportjs(jsfile);
					
					std::stringstream().swap(s);
					jsfile.close();
					
					mvaddstr(topoffset+2+entries+4,1, "Script generated!");
				}
				break;
			}
			case 113:  // ch is 'q'
			case 81:  // ch is 'Q'
				HAS_QUIT=true;
				return;
				break;
		}
		refresh();
	}
}
void modeselectscr_update(int selrow, int topoffset, int curlin, std::vector<std::string> &options) {
	erase(); refresh(); // erase screen to redraw everything based on user selection in caller function
	
	printHead();
	
	mvaddstr(topoffset+2,1, "Mode select:");
	
	int entries = 0; // file entries counter, to set visual properties of printed rows
	
	// print options found, one per row
	
	for(int i=0 ; i<options.size() ; i++) {
		if(entries == selrow) {   // if current entry is selected BUT NOT highlighted
			attron(COLOR_PAIR(2)); // set color pair as black(foreground) and white(background)
			mvaddstr(curlin++,4, options[i].c_str()); // ...
			attroff(COLOR_PAIR(2)); // ...
		}
		else   // if neither selected nor highlighted
			mvaddstr(curlin++,4, options[i].c_str()); // print with default color pair
		
		entries++;
	}
	
	mvaddstr(topoffset+2+entries+2,1, "[Press q to quit]");
	
	refresh();
}

void inputsetupscr(std::string instructions) {
	int selcolumn = 0;
	int topoffset = 11;
	
	int promptnum = 1;
	for (int i = 0; i < columncount; i++) {
		hazards.emplace_back("---");
    }
	
	erase(); refresh();
	
	printHead();
	
	mvaddstr(topoffset-3,1, instructions.c_str());
	
	int numofrows = 7;
	numofrows = (numofrows > rowcount) ? rowcount : numofrows;  // make sure numofrows requested are <= available rows
	int columnwidths[columncount];
	printSpreadsheetPromptOrder(topoffset, numofrows, columnwidths, selcolumn);
	
	int nthtograde=1; // for storing column grading order setup
	int ch; // stores user input in int
	while(ch = getch()) {   // keep getting user input, as long as he hasn't selected Quit option, which would return to main()
		switch(ch) {
			case KEY_RIGHT:
				if(selcolumn == columncount-1) {}
				else {
					selcolumn++;
					if(selcolumn > rightmostcolumn) { 
						rightmostcolumn++;
						leftmostcolumn++;
					}
					inputsetupscr_update(selcolumn, numofrows, instructions);
				}
				break;
			case KEY_LEFT:
				if(selcolumn == 0) {}
				else {
					selcolumn--;
					if(selcolumn < leftmostcolumn) { 
						rightmostcolumn--;
						leftmostcolumn--;
					}
					inputsetupscr_update(selcolumn, numofrows, instructions);
				}
				break;
			case 10: {  // if ch is ENTER key
				if(promptnum == 1) {
					idcolumn = selcolumn; 
					promptnum = 2;
					hazards[selcolumn] = "ID";
					instructions = "Use the < > arrow keys and press enter on the STUDENT NAME column:";
					inputsetupscr_update(selcolumn, numofrows, instructions);
				}
				else if(promptnum == 2) {
					namecolumn = selcolumn;
					promptnum = 3;
					hazards[selcolumn] = "NAME";
					instructions = "Use the < > arrow keys and press enter on the columns TO-GRADE, in the ORDER you want to enter them in, finishing with [G/g]:";
					inputsetupscr_update(selcolumn, numofrows, instructions);
				}
				else {
					hazards[selcolumn] = std::__cxx11::to_string(nthtograde);
					columnstograde.emplace_back(selcolumn);
					nthtograde++;
					promptnum = 4;
					inputsetupscr_update(selcolumn, numofrows, instructions);
				}
				break;
			}
			case 103:  // ch is 'g'
			case 71:  // ch is 'G'
				switch(promptnum) {
					case 1:
						attron(COLOR_PAIR(6));
						mvaddstr(topoffset-2,1, "Oh, not so fast... Set up the ID column first!");
						attroff(COLOR_PAIR(6));
						break;
					case 2:
						attron(COLOR_PAIR(6));
						mvaddstr(topoffset-2,1, "Slow down... Set up the NAME column first!");
						attroff(COLOR_PAIR(6));
						break;
					case 3:
						attron(COLOR_PAIR(6));
						mvaddstr(topoffset-2,1, "Ehm ehm... At least one TO-GRADE column first!");
						attroff(COLOR_PAIR(6));
						break;
					case 4:
						showWin=2;
						return;
						break;
				
				}
				break;
			case 113:  // ch is 'q'
			case 81:  // ch is 'Q'
				HAS_QUIT=true;
				return;
				break;
		}
		refresh();
	}
	
}

void inputsetupscr_update(int selcolumn, int numofrows, std::string instructions) {
	int topoffset = 11;
	
	erase(); refresh();
	
	printHead();
	
	mvaddstr(topoffset-3,1, instructions.c_str());
	
	int columnwidths[columncount];
	printSpreadsheetPromptOrder(topoffset, numofrows, columnwidths, selcolumn);
	
}

void printSpreadsheetPromptOrder(int& offset, int& numofrows, int* columnwidths, int& selcolumn) {
	
	int row = 0;
	
	int curcolumn = leftmostcolumn;
	
	for( ; curcolumn < columncount ; curcolumn++) {  // set columnwidths for each column, used for centering later
		int tmpwidth = 0;
		
		for(; row < numofrows ; row++) {
			if(cellwidths[row*columncount+curcolumn] > tmpwidth) {
				tmpwidth = cellwidths[row*columncount+curcolumn];
			}
		}
		columnwidths[curcolumn] = tmpwidth;
		row = 0;
	}
	
	int columnoffset = 2;
	for(row = 0 ; row < numofrows ; row++) {
		mvaddstr(offset,columnoffset, "|");
		for(curcolumn = leftmostcolumn ; curcolumn <= rightmostcolumn ; curcolumn++) {
			int middler = ((columnwidths[curcolumn] + 2) - cellwidths[row*columncount+curcolumn]);
			int start = (middler % 2 == 0) ? start = middler / 2 : (middler - 1) / 2;
			
			if(curcolumn == selcolumn) {attron(COLOR_PAIR(4));};
			
			mvaddstr(offset,columnoffset+start, spreadsheet[row*columncount+curcolumn].c_str());
			attroff(COLOR_PAIR(4));
			mvaddstr(offset,columnoffset+columnwidths[curcolumn]+1, "|");		
			attron(COLOR_PAIR(5));
			mvaddstr(offset,columnoffset+columnwidths[curcolumn]+2, "o");
			attroff(COLOR_PAIR(5));
			
			columnoffset += columnwidths[curcolumn] + 2;
		}
		
		curcolumn = leftmostcolumn;
		columnoffset = 2;
		offset++;
	}
	
	offset += 3;
	for(curcolumn = leftmostcolumn ; curcolumn <= rightmostcolumn ; curcolumn++) {
		int middler = ((columnwidths[curcolumn] + 2) - hazards[curcolumn].length());
		int start = (middler % 2 == 0) ? start = middler / 2 : (middler - 1) / 2;
		
		if(hazards[curcolumn].compare("ID") == 0 || hazards[curcolumn].compare("NAME") == 0) { attron(COLOR_PAIR(4)); }
		else if(hazards[curcolumn].compare("---") != 0) { attron(COLOR_PAIR(2)); }
		mvaddstr(offset,columnoffset+start, hazards[curcolumn].c_str());
		attroff(COLOR_PAIR(4));
		mvaddstr(offset,columnoffset+columnwidths[curcolumn]+1, " ");		
		attron(COLOR_PAIR(5));
		mvaddstr(offset,columnoffset+columnwidths[curcolumn]+2, "o");
		attroff(COLOR_PAIR(5));
			
		columnoffset += columnwidths[curcolumn] + 2;
	}
	
	mvaddstr(offset+4,1, "[Press q to quit]");
	
	offset=11;
	
	refresh();
}

void gradingscr() {
	erase(); refresh();
	printHead();
	
	int topoffset = 8;
	
	int input;
	std::stringstream s;
	int counter = 0;
	int numofrows = 9;
	numofrows = (numofrows < rowcount) ? numofrows : rowcount;
	
	//first data row selection
	mvaddstr(topoffset,1, "Select the row # of the first data entry: ");
	int firstdatarow;
	int selrow = 0;
	printFirstRowSelector(topoffset+5, numofrows, selrow);
	do {
        input = getch();
        switch(input) {
			case KEY_UP: 
				if(selrow > 0) {
					selrow--;
					printFirstRowSelector(topoffset+5, numofrows, selrow);
				}
				break;
			case KEY_DOWN:
				if(selrow < numofrows-1) {
					selrow++;
					printFirstRowSelector(topoffset+5, numofrows, selrow);
				}
				break;
			case 10:
				firstdatarow = selrow;
				std::stringstream().swap(s);
				break;
			case 113:
			case 81:
				HAS_QUIT = true;
				return;
				break;
		}
    } while(input != 10);
    
    counter=0;
    selrow=0;
    
    //search for student
    erase(); refresh();
	printHead();
	std::string studentid;
	std::stringstream().swap(s);
	std::vector<int> foundones;
	s << "Search for the good old \"LAST 4 DIGITS\":     [s]ave to <MODIFIED_" << filename 
      << ">\n                                              [o]verwrite <" << filename << ">  [q]uit";
	std::string instructions = s.str();
	std::stringstream().swap(s);
	mvaddstr(topoffset,1, instructions.c_str());
	mvaddstr(topoffset+3,1, " ->  [      ] ");
	do {
		input = getch();
        if(input > 47 && input < 58 && counter < 4) {
			s << input-48;
			mvaddstr(topoffset+3, 8, s.str().c_str());
            counter++;
        }
        if(input == 10) {
			s >> studentid;
			foundones = studentsfound(studentid);
			if(foundones.size() < 1) {                         // ///////check for find
				std::stringstream().swap(s);
				mvaddstr(topoffset+3,1, " ->  [      ] ");
				counter=0;
			}
			else if(foundones.size() > 1) {                                       
				printstudentselector(topoffset, foundones, selrow);
				
				do {
					input = getch();
					switch(input) {
						case KEY_UP: 
							if(selrow > 0) {
								selrow--;
								printstudentselector(topoffset, foundones, selrow);
							}
							break;
						case KEY_DOWN:
							if(selrow < foundones.size()-1) {
								selrow++;
								printstudentselector(topoffset, foundones, selrow);
							}
							break;
						case 10:
							printstudentgrader(foundones[selrow], firstdatarow);
							erase(); refresh();
							printHead();
							mvaddstr(topoffset,1, instructions.c_str());
							mvaddstr(topoffset+3,1, " ->  [      ] ");
							break;
						case 113:
						case 81:
							HAS_QUIT = true;
							return;
							break;
					}
				} while(input != 10);
				counter=0;
				std::stringstream().swap(s);
				selrow=0;
			}
			else {
				printstudentgrader(foundones[0], firstdatarow);
				counter=0;
				std::stringstream().swap(s);
				selrow=0;
				erase(); refresh();
				printHead();
				mvaddstr(topoffset,1, instructions.c_str());
				mvaddstr(topoffset+3,1, " ->  [      ] ");
			}
        }
        else if (input == KEY_BACKSPACE) {
			std::string backspaceinput = s.str();
			if (!backspaceinput.empty()) {
				backspaceinput.pop_back();
				std::stringstream().swap(s);
				s << backspaceinput;
				counter--;
				erase();
				printHead();
				mvaddstr(topoffset,1, instructions.c_str());
				mvaddstr(topoffset+3,1, " ->  [      ] ");
				mvaddstr(topoffset+3, 8, s.str().c_str());
			}
		}
		else if (input == 115 || input == 83) {
			exportcsv(1);
		}
		else if (input == 111 || input == 79) {
			exportcsv(2);
		}
		else if (input == 113 || input == 81) {
			HAS_QUIT=true;
			return;
		}
        refresh();
    } while(1);
    
}

void printFirstRowSelector(int offset, int numofrows, int &selrow) {
	int row = 0;
	int columnwidths[columncount];
	int curcolumn = leftmostcolumn;
	
	for( ; curcolumn < columncount ; curcolumn++) {  // set columnwidths for each column, used for centering later
		int tmpwidth = 0;
		
		for(; row < numofrows ; row++) {
			if(cellwidths[row*columncount+curcolumn] > tmpwidth) {
				tmpwidth = cellwidths[row*columncount+curcolumn];
			}
		}
		columnwidths[curcolumn] = tmpwidth;
		row = 0;
	}
	
	int columnoffset = 2;
	for(row = 0 ; row < numofrows ; row++) {
		if(row == selrow) {
			attron(COLOR_PAIR(2));
		}
		mvaddstr(offset,columnoffset, std::__cxx11::to_string(row+1).c_str());
		mvaddstr(offset,columnoffset+2, " --> ");
		attroff(COLOR_PAIR(2));
		mvaddstr(offset,columnoffset+7, "|");
		for(curcolumn = leftmostcolumn ; curcolumn <= rightmostcolumn+1 ; curcolumn++) {
			int middler = ((columnwidths[curcolumn] + 2) - cellwidths[row*columncount+curcolumn]);
			int start = (middler % 2 == 0) ? start = middler / 2 : (middler - 1) / 2;
			
			mvaddstr(offset,columnoffset+7+start, spreadsheet[row*columncount+curcolumn].c_str());
			mvaddstr(offset,columnoffset+7+columnwidths[curcolumn]+1, "|");		
			attron(COLOR_PAIR(5));
			mvaddstr(offset,columnoffset+7+columnwidths[curcolumn]+2, "o");
			attroff(COLOR_PAIR(5));
			
			columnoffset += columnwidths[curcolumn] + 2;
		}
		
		curcolumn = leftmostcolumn;
		columnoffset = 2;
		offset++;
	}
	refresh();
}

void printstudentselector(int &topoffset, std::vector<int> &foundones, int &selrow) {
	erase();
	printHead();
	
	mvaddstr(topoffset,1, "Many students found with the same last 4. Select the needed: ");
	
	for(int i=0 ; i < foundones.size() ; i++) {
		if(i == selrow) attron(COLOR_PAIR(2));
		mvaddstr(topoffset+2+i,1, spreadsheet[foundones[i]*columncount+namecolumn].c_str());
		attroff(COLOR_PAIR(2));
	}
	refresh();
}

static bool endsWith(const std::string& str, const char* suffix, unsigned suffixLen) {
	return str.size() >= suffixLen && 0 == str.compare(str.size()-suffixLen, suffixLen, suffix, suffixLen);
}

static bool startsWith(const std::string& str, const char* prefix, unsigned prefixLen) {
    return str.size() >= prefixLen && 0 == str.compare(0, prefixLen, prefix, prefixLen);
}

std::vector<int> studentsfound(std::string id) {
	std::vector<int> foundones;
	for(int row = 0 ; row < rowcount ; row++) {
		if(endsWith(spreadsheet[row*columncount+idcolumn], id.c_str(), std::string::traits_type::length(id.c_str())))
			foundones.emplace_back(row);
	}
	return foundones;
}

void printstudentgrader(int idrow, int firstdatarow) {
	erase(); refresh();
	printHead();
	
	int topoffset = 8;
	int graderpusher;
	
	int input;
	std::stringstream s;
	
	for(int i = 0 ; i < columnstograde.size() ; i++) {
		erase(); refresh();
		printHead();
		mvaddstr(topoffset+1, 2, spreadsheet[idrow*columncount+idcolumn].c_str());
		mvaddstr(topoffset+2, 2, spreadsheet[idrow*columncount+namecolumn].c_str());
		
		for(int j = 0 ; j < firstdatarow ; j++) {
			mvaddstr(topoffset+6+j, 2, spreadsheet[j*columncount+columnstograde[i]].c_str());
		}
		mvaddstr(topoffset+6+firstdatarow, 2, spreadsheet[idrow*columncount+columnstograde[i]].c_str());
		graderpusher = spreadsheet[idrow*columncount+columnstograde[i]].size();
		mvaddstr(topoffset+6+firstdatarow, graderpusher+4, "<<");
		
		//take grade enter
		do {
			input = getch();
			if(input > 47 && input < 58) {
				s << input-48;
				mvaddstr(topoffset+6+firstdatarow, graderpusher+8, s.str().c_str());
			}
			else if(input == '.') {
				s << '.';
				mvaddstr(topoffset+6+firstdatarow, graderpusher+8, s.str().c_str());
				
			}
			else if(input == 10) {
				s >> spreadsheet[idrow*columncount+columnstograde[i]];
				std::stringstream().swap(s);
				break;
			}
			else if (input == KEY_BACKSPACE) {
				std::string backspaceinput = s.str();
				if (!backspaceinput.empty()) {
					backspaceinput.pop_back();
					std::stringstream().swap(s);
					s << backspaceinput;
					mvaddstr(topoffset+6+firstdatarow, graderpusher+8, "               ");
					mvaddstr(topoffset+6+firstdatarow, graderpusher+8, s.str().c_str());
					refresh();
				}
			}
			else if (input == 113 || input == 81) {
				std::stringstream().swap(s);
				HAS_QUIT=true;
				return;
			}
			refresh();
		} while(1);
		
	}
}

void exportcsv(int option) {
	std::stringstream s;
	std::ofstream file;
	
	if(option == 1) {
		s << "MODIFIED_" << filename;
		file.open(s.str());
		std::stringstream().swap(s);
	}
	else { file.open(filename); }
		
	for(int row=0 ; row < rowcount ; row++) {
		for(int column=0 ; column < columncount ; column++) {
			s << spreadsheet[row*columncount+column];
			if(column < columncount-1) s << delimiter;
		}
		s << "\n";
	}
	
	file << s.str();
	
	file.close();
}

void exportjs(std::ofstream& file) {
	std::stringstream s;
	s << "var numcolumn = " << columncount << ", numstudents = " << rowcount << ";\n\n"
	  << "// |    0     |  1 |       2      |    3     |   4   |   5\n"
	  << "// | tasalsul | id | student name | semester | final | total\n"
	  << "const spreadsheet = [";
	  
	for(int row=0 ; row < rowcount ; row++) {
		for(int column=0 ; column < columncount ; column++) {
			if(column == 2) s << '\"';
			s << spreadsheet[row*columncount+column];
			if(column == 2) s << '\"';
			if(column < columncount-1) s << ",";
		}
		if(row == rowcount-1) { 
			s << "];\n\n"; 
		}
		else { 
			s << ",\n  		     ";
		}
	}
	
	s << "var elements = document.getElementsByTagName(\"span\"), id = \"myForm:students:\";\n\n"
	  << "for(var student = 0; student < numstudents; student++) {\n"
	  << "	if(document.getElementById(id.concat(student, \":studentId\")) != null) {\n"
	  << "		if(document.getElementById(id.concat(student, \":mark0\")) != null) {\n"
	  << "			document.getElementById(id.concat(student, \":mark0\")).value = spreadsheet[student*numcolumn+3];\n"
	  << "		}\n"
	  << "		if(document.getElementById(id.concat(student, \":mark1\")) != null) {\n"
	  << "			document.getElementById(id.concat(student, \":mark1\")).value = spreadsheet[student*numcolumn+4];\n"
	  << "		}\n"
	  << "		if(document.getElementById(id.concat(student, \":totalMark\")) != null) {\n"
	  << "			document.getElementById(id.concat(student, \":totalMark\")).value = spreadsheet[student*numcolumn+5];\n"
	  << "		}\n"
	  << "	}\n"
	  << "}\n";
	  
	file << s.str();
}
